find_program(CLANG_TIDY_EXECUTABLE NAMES clang-tidy) # Search for clang-tidy on system
mark_as_advanced(CLANG_TIDY_EXECUTABLE) # Don't show Variable in gui tools ccmake, cmake

if (ENABLE_LINTING)
    if (${CLANG_TIDY_EXECUTABLE})
        message(WARNING "Clang-tidy not found")
    else()
        message(STATUS "Enabling clang-tidy")
        set(CMAKE_CXX_CLANG_TIDY "${CLANG_TIDY_EXECUTABLE};--checks=misc-unused-parameters")
    endif()
endif()
