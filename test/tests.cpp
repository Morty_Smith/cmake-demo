#define CATCH_CONFIG_MAIN

#include "foo.hpp"
#include "baz.hpp"
#include "bar.hpp"

#include <catch2/catch.hpp>

TEST_CASE("foo is foo") {
    REQUIRE("foo" == foo::text());
}

TEST_CASE("bar is bar") {
    REQUIRE("bar" == bar::text());
}

TEST_CASE("baz is baz") {
    REQUIRE("baz" == baz::text());
}
