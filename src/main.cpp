#include "bar.hpp"
#include "baz.hpp"
#include "foo.hpp"

#include <iostream>

int main(int argc, char** argv) {
    int i = 1;
    std::cout << foo::text() << bar::text() << ", " << baz::text() << "\n";
    std::cout << "Build Type: " << TYPE << "\n";

}
